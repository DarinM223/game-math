use crate::vector::Vector3D;
use std::ops;

#[derive(Clone)]
pub struct Matrix3D([[f32; 3]; 3]);

impl Matrix3D {
    pub fn new(matrix: [[f32; 3]; 3]) -> Matrix3D {
        let n = [
            [matrix[0][0], matrix[1][0], matrix[2][0]],
            [matrix[0][1], matrix[1][1], matrix[2][1]],
            [matrix[0][2], matrix[1][2], matrix[2][2]],
        ];

        Matrix3D(n)
    }

    pub fn get(&self, i: usize, j: usize) -> f32 {
        self.0[j][i]
    }

    pub fn get_vector(&self, j: usize) -> Vector3D {
        Vector3D {
            x: self.0[j][0],
            y: self.0[j][1],
            z: self.0[j][2],
        }
    }

    pub fn determinant(&self) -> f32 {
        self.get(0, 0) * self.get(1, 1) * self.get(2, 2) - self.get(1, 2) * self.get(2, 1)
            + self.get(0, 1) * self.get(1, 2) * self.get(2, 0)
            - self.get(1, 0) * self.get(2, 2)
            + self.get(0, 2) * self.get(1, 0) * self.get(2, 1)
            - self.get(1, 1) * self.get(2, 0)
    }

    pub fn inverse(&self) -> Matrix3D {
        let a = self.get_vector(0);
        let b = self.get_vector(1);
        let c = self.get_vector(2);

        let r0 = b.cross(&c);
        let r1 = c.cross(&a);
        let r2 = a.cross(&b);

        let inv_det = 1. / r2.dot(&c);

        Matrix3D::new([
            [r0.x * inv_det, r0.y * inv_det, r0.z * inv_det],
            [r1.x * inv_det, r1.y * inv_det, r1.z * inv_det],
            [r2.x * inv_det, r2.y * inv_det, r2.z * inv_det],
        ])
    }
}

impl ops::Add<Matrix3D> for Matrix3D {
    type Output = Matrix3D;

    fn add(self, other: Matrix3D) -> Matrix3D {
        Matrix3D([
            [
                self.0[0][0] + other.0[0][0],
                self.0[0][1] + other.0[0][1],
                self.0[0][2] + other.0[0][2],
            ],
            [
                self.0[1][0] + other.0[1][0],
                self.0[1][1] + other.0[1][1],
                self.0[1][2] + other.0[1][2],
            ],
            [
                self.0[2][0] + other.0[2][0],
                self.0[2][1] + other.0[2][1],
                self.0[2][2] + other.0[2][2],
            ],
        ])
    }
}

impl ops::Mul<f32> for Matrix3D {
    type Output = Matrix3D;

    fn mul(self, i: f32) -> Matrix3D {
        Matrix3D::new([
            [self.0[0][0] * i, self.0[0][1] * i, self.0[0][2] * i],
            [self.0[1][0] * i, self.0[1][1] * i, self.0[1][2] * i],
            [self.0[2][0] * i, self.0[2][1] * i, self.0[2][2] * i],
        ])
    }
}

impl ops::Sub<Matrix3D> for Matrix3D {
    type Output = Matrix3D;

    fn sub(self, other: Matrix3D) -> Matrix3D {
        self + other * (-1.)
    }
}

impl ops::Mul<Matrix3D> for Matrix3D {
    type Output = Matrix3D;

    fn mul(self, other: Matrix3D) -> Matrix3D {
        Matrix3D::new([
            [
                self.get(0, 0) * other.get(0, 0)
                    + self.get(0, 1) * other.get(1, 0)
                    + self.get(0, 2) * other.get(2, 0),
                self.get(0, 0) * other.get(0, 1)
                    + self.get(0, 1) * other.get(1, 1)
                    + self.get(0, 2) * other.get(2, 1),
                self.get(0, 0) * other.get(0, 2)
                    + self.get(0, 1) * other.get(1, 2)
                    + self.get(0, 2) * other.get(2, 2),
            ],
            [
                self.get(1, 0) * other.get(0, 0)
                    + self.get(1, 1) * other.get(1, 0)
                    + self.get(1, 2) * other.get(2, 0),
                self.get(1, 0) * other.get(0, 1)
                    + self.get(1, 1) * other.get(1, 1)
                    + self.get(1, 2) * other.get(2, 1),
                self.get(1, 0) * other.get(0, 2)
                    + self.get(1, 1) * other.get(1, 2)
                    + self.get(1, 2) * other.get(2, 2),
            ],
            [
                self.get(2, 0) * other.get(0, 0)
                    + self.get(2, 1) * other.get(1, 0)
                    + self.get(2, 2) * other.get(2, 0),
                self.get(2, 0) * other.get(0, 1)
                    + self.get(2, 1) * other.get(1, 1)
                    + self.get(2, 2) * other.get(2, 1),
                self.get(2, 0) * other.get(0, 2)
                    + self.get(2, 1) * other.get(1, 2)
                    + self.get(2, 2) * other.get(2, 2),
            ],
        ])
    }
}

impl ops::Mul<Vector3D> for Matrix3D {
    type Output = Vector3D;

    fn mul(self, v: Vector3D) -> Vector3D {
        Vector3D {
            x: self.get(0, 0) * v.x + self.get(0, 1) * v.y + self.get(0, 2) * v.z,
            y: self.get(1, 0) * v.x + self.get(1, 1) * v.y + self.get(1, 2) * v.z,
            z: self.get(2, 0) * v.x + self.get(2, 1) * v.y + self.get(2, 2) * v.z,
        }
    }
}

pub fn rotation_x(t: f32) -> Matrix3D {
    let c = t.cos();
    let s = t.sin();
    Matrix3D::new([[1., 0., 0.], [0., c, -s], [0., s, c]])
}

pub fn rotation_y(t: f32) -> Matrix3D {
    let c = t.cos();
    let s = t.sin();
    Matrix3D::new([[c, 0., s], [0., 1., 0.], [-s, 0., c]])
}

pub fn rotation_z(t: f32) -> Matrix3D {
    let c = t.cos();
    let s = t.sin();
    Matrix3D::new([[c, -s, 0.], [s, c, 0.], [0., 0., 1.]])
}

pub fn rotation_about_vector(t: f32, a: &Vector3D) -> Matrix3D {
    let c = t.cos();
    let s = t.sin();
    let d = 1. - c;
    let (x, y, z) = (a.x * d, a.y * d, a.z * d);
    let (axay, axaz, ayaz) = (x * a.y, x * a.z, y * a.z);
    Matrix3D::new([
        [c + x * a.x, axay - s * a.z, axaz + s * a.y],
        [axay + s * a.z, c + y * a.y, ayaz - s * a.x],
        [axaz - s * a.y, ayaz + s * a.x, c + z * a.z],
    ])
}

pub fn reflection(a: &Vector3D) -> Matrix3D {
    let (x, y, z) = (a.x * -2., a.y * -2., a.z * -2.);
    let (axay, axaz, ayaz) = (x * a.y, x * a.z, y * a.z);
    Matrix3D::new([
        [x * a.x + 1., axay, axaz],
        [axay, y * a.y + 1., ayaz],
        [axaz, ayaz, z * a.z + 1.],
    ])
}

pub fn involution(a: &Vector3D) -> Matrix3D {
    let (x, y, z) = (a.x * 2., a.y * 2., a.z * 2.);
    let (axay, axaz, ayaz) = (x * a.y, x * a.z, y * a.z);
    Matrix3D::new([
        [x * a.x - 1., axay, axaz],
        [axay, y * a.y - 1., ayaz],
        [axaz, ayaz, z * a.z - 1.],
    ])
}

pub fn scale(sx: f32, sy: f32, sz: f32) -> Matrix3D {
    Matrix3D::new([[sx, 0., 0.], [0., sy, 0.], [0., 0., sz]])
}

pub fn scale_by_direction(s: f32, a: &Vector3D) -> Matrix3D {
    let s = s - 1.;
    let (x, y, z) = (a.x * s, a.y * s, a.z * s);
    let (axay, axaz, ayaz) = (x * a.y, x * a.z, y * a.z);
    Matrix3D::new([
        [x * a.x + 1., axay, axaz],
        [axay, y * a.y + 1., ayaz],
        [axaz, ayaz, z * a.z + 1.],
    ])
}
