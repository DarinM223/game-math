use std::ops;

#[derive(Clone)]
pub struct Vector3D {
    pub x: f32,
    pub y: f32,
    pub z: f32,
}

impl Vector3D {
    pub fn new(x: f32, y: f32, z: f32) -> Vector3D {
        Vector3D { x, y, z }
    }

    pub fn magnitude(&self) -> f32 {
        (self.x.powf(2.) + self.y.powf(2.) + self.z.powf(2.)).sqrt()
    }

    pub fn normalize(&self) -> Vector3D {
        self.clone() / self.magnitude()
    }

    pub fn dot(&self, other: &Vector3D) -> f32 {
        self.x * other.x + self.y * other.y + self.z * other.z
    }

    pub fn cross(&self, other: &Vector3D) -> Vector3D {
        Vector3D {
            x: self.y * other.z - self.z * other.y,
            y: self.z * other.x - self.x * other.z,
            z: self.x * other.y - self.y * other.x,
        }
    }

    pub fn project(&self, other: &Vector3D) -> Vector3D {
        other.clone() * (self.dot(other) / other.dot(other))
    }

    pub fn reject(&self, other: &Vector3D) -> Vector3D {
        self.clone() - other.clone() * (self.dot(other) / other.dot(other))
    }
}

impl ops::Index<usize> for Vector3D {
    type Output = f32;

    fn index(&self, index: usize) -> &f32 {
        match index {
            0 => &self.x,
            1 => &self.y,
            2 => &self.z,
            _ => panic!("Invalid index"),
        }
    }
}

impl ops::AddAssign<Vector3D> for Vector3D {
    fn add_assign(&mut self, other: Vector3D) {
        *self = self.clone() + other;
    }
}

impl ops::SubAssign<Vector3D> for Vector3D {
    fn sub_assign(&mut self, other: Vector3D) {
        *self = self.clone() - other;
    }
}

impl ops::MulAssign<f32> for Vector3D {
    fn mul_assign(&mut self, s: f32) {
        *self = self.clone() * s;
    }
}

impl ops::DivAssign<f32> for Vector3D {
    fn div_assign(&mut self, s: f32) {
        *self = self.clone() * (1.0 / s)
    }
}

impl ops::Mul<f32> for Vector3D {
    type Output = Vector3D;

    fn mul(self, s: f32) -> Vector3D {
        Vector3D {
            x: self.x * s,
            y: self.y * s,
            z: self.z * s,
        }
    }
}

impl ops::Div<f32> for Vector3D {
    type Output = Vector3D;

    fn div(self, s: f32) -> Vector3D {
        let s = 1.0 / s;
        self * s
    }
}

impl ops::Neg for Vector3D {
    type Output = Vector3D;

    fn neg(self) -> Vector3D {
        Vector3D {
            x: -self.x,
            y: -self.y,
            z: -self.z,
        }
    }
}

impl ops::Add<Vector3D> for Vector3D {
    type Output = Vector3D;

    fn add(self, other: Vector3D) -> Vector3D {
        Vector3D {
            x: self.x + other.x,
            y: self.y + other.y,
            z: self.z + other.z,
        }
    }
}

impl ops::Sub<Vector3D> for Vector3D {
    type Output = Vector3D;

    fn sub(self, other: Vector3D) -> Vector3D {
        Vector3D {
            x: self.x - other.x,
            y: self.y - other.y,
            z: self.z - other.z,
        }
    }
}

impl ops::Mul<Vector3D> for Vector3D {
    type Output = Vector3D;

    fn mul(self, other: Vector3D) -> Vector3D {
        Vector3D {
            x: self.x * other.x,
            y: self.y * other.y,
            z: self.z * other.z,
        }
    }
}
